## :label: CBIR with calebA dataset

Find out which celebrity you look most like.

## :mag: Content

- [Overview](#overview)
- [Used Dataset](#dataset)
- [Local environment setup](#local-env-setup)
- [Steps](#steps)
- [Fine-tuning MobileNet V2](#finetuning)

## <a name="overview">:globe_with_meridians: Overview</a>

This project uses pre-trained [MobieNet V2](https://arxiv.org/pdf/1704.04861.pdf) to get feature vectors from images. You can download this feature extractor and see more information about it [here](https://tfhub.dev/google/imagenet/mobilenet_v2_140_224/feature_vector/5). 

## <a name="dataset">:file_folder: Dataset</a>

The **celebA** dataset was used as base dataset of celebrities. This dataset contains about 200k images. In this problem only subset of 20k images is used.

## <a name="local-env-setup">:computer: Local environment setup</a>
1. Install mamba to your base conda installation: `conda install mamba -n base -c conda-forge` if not installed jet.
1. Download the app from this GitLab repo.
1. Use mamba ENV Manager to create new ENV: `mamba env create -f environment.yml` in the root directory.
1. Activate the ENV: `conda activate cbir-mobilenet`

## <a name="steps">:heavy_check_mark: Steps</a>

1. Get database with images - you can choose from two ways:
	1. Download and extract images from [here](https://cbir-data.s3.amazonaws.com/database_20k.zip) and save them to folder *repo/database_20k*. In this case you can use already extracted feature vectors stored in *repo/features* and you will not have to wait for them to be extracted by model. There are two files with features:
		* *index.h5* - features from original mobilenet 
		* *index_test.h5* - features from fine-tuned mobilnet *repo/fine_tuning_mobilenet/models/fine_tuned_1_20210622_133934* 
	2. Second choice is to download the whole **celebA** dataset from [kaggle](https://www.kaggle.com/jessicali9530/celeba-dataset) - about 1Gb. You will need **img_align_celeba** folder. (For fine-tuning you will also need list_attr_celeba.csv - but it is already in *repo/fine_tuning_mobilenet*).<br/>Create subset of all images from just downloaded dataset using **select_sample.py** (see below) in *repo/modules* and save these images in *repo/database_20k*.<br/>

2. Choose your query image and follow steps in **cbir.ipynb** (for testing purposes you can use image in **query** folder) - if you chose to follow step 2. in previous step, then this step will take about 15-30min for the first time because feature vectors have to be calculated by model.  


#### Using select_sample.py:

select_sample.py has 3 arguments:
- ```-s --source-path```: path to folder with celebA images 
- ```-t --target-path```: path where to save sample of images
- ```-n --n-sam```: size of sample, default = 20000 


``` python 
python modules/select_sample.py -s path_to_all_images -t repo/database_20k -n 20000  
```

## <a name="finetuning">:massage: Fine-tuning MobileNet V2</a>

When looking for similar images you can choose whether to use the original MobileNet V2 or you can use your fine-tuned MobileNet CNN. 
If you decide for fine-tuning CNN follow the steps in jupyter notebook **repo/fine_tuning_mobilenet/fine_tuning_mobilenet.ipynb** or you can use already fine-tuned mobileNet *repo/fine_tuning_mobilenet/models/fine_tuned_1_20210622_133934* and features *index_test.h5* as shown in **cbir.ipynb**.

#### How to use your fine-tuned mobileNet in **cbir.ipynb**:
```python
mobilenet_FT = MobileNet('path_to_saved_fine_tuned_model')
cbir_query.show_similar_imgs('query_img_path', mobilenet=mobilenet_FT, feature_path = 'path_to_save_load_features', dp_path = 'database_20k')
```

For example usage of  fine-tuned mobileNet in this repo and given feature vectors:

```python
mobilenet_FT = MobileNet('fine_tuning_mobilenet/models/fine_tuned_1_20210622_133934')
cbir_query.show_similar_imgs('query/IMG_9832.jpg', mobilenet=mobilenet_FT, feature_path = 'features/index_test.h5', dp_path = 'database_20k')
```
