import logging
from pathlib import Path
from typing import Union
import tensorflow as tf
import tensorflow_hub as tf_hub
from tensorflow.python.framework.ops import EagerTensor

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


class MobileNet:
    """
    MobileNet class. When invoking this class you can use your own
    finetuned mobilenet by providing path to them. Or default feature
    vectors of images with MobileNet V2 trained on ImageNet.
    """
    def __init__(self, model_path: Union[str, Path] = None):
        """
        Define mobilenet CNN

        Args:
            model_path (Union[str, Path], optional):
                Path to finetune mobilenet model. If 'None' then modul from
                https://tfhub.dev/google/imagenet/mobilenet_v2_140_224/feature_vector/5
                will be used. Defaults to None.
        """

        self.input_shape = (224, 224, 3)
        self.model_path = model_path

        if model_path is None:
            self.modul = "https://tfhub.dev/google/imagenet/mobilenet_v2_140_224/feature_vector/5"
            self.model = tf_hub.load(self.modul)
            self.mobilenet_type = "ORIG"
        else:
            self.modul = tf.keras.models.load_model(str(model_path))
            self.model = tf.keras.models.Sequential(self.modul.layers[:-1])
            self.mobilenet_type = "FN"  # finetuned

    def extract_feat(self, img_path: str) -> EagerTensor:
        """
        The mobilenet CNN requires 1 x 224 x 224 x 3 tensor with float32 so
        first load and decode image to W x H x 3 tensor then
        Resize to 224 x 224 x 3 tensor and preprocess with mobilenet preprocessing
        function and then convert unit8 to float32. After that we cen get the
        feature vector for the image.

        Args:
            img_path (str): path of image to extract feature vector of

        Returns:
            EagerTensor: picture featrue vector
        """

        img = tf.io.read_file(img_path)
        img = tf.io.decode_jpeg(img, channels=self.input_shape[2])

        img = tf.image.resize_with_pad(img, self.input_shape[0], self.input_shape[1])

        img = tf.keras.applications.mobilenet.preprocess_input(img)
        img = tf.image.convert_image_dtype(img, tf.float32)[tf.newaxis, ...]

        feat = self.model(img)

        return feat
