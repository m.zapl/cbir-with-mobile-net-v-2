import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.style as style
import numpy as np
from typing import Tuple, Union
from tensorflow.python.framework.ops import EagerTensor
from tensorflow.python.data.ops.dataset_ops import PrefetchDataset
from keras.callbacks import History
from pathlib import Path


def prep_img(
    img_path: Union[str, Path],
    is_tuning: bool = True,
    input_shape: tuple = (224, 224, 3)
) -> EagerTensor:
    """
    It Loads the jpg from the given path
    Decodes the image to a uint8 W X H X 3 and
    resizes the image to 224 x 224 x 3 tensor.
    If image will not be used for fine tuning mobile
    net the tensor will be reshapet to 1 x 224 x 224 x 3
    before mobilenet preprocessing.

    Args:
        img_path (str): path to image
        is_tuning (bool, optional): Boolean to indicate if
            image will be used for fine tuning.
            Defaults to True.
        input_shape (tuple, optional): Shape of image the model expect.
            Defaults to (224,224,3).

    Returns:
        EagerTensor: tensor repre. of image
    """
    # tf.io.read_file expects string object
    img_path = str(img_path)
    # Load and decode image to W x H x 3 tensor
    img = tf.io.read_file(img_path)
    img = tf.io.decode_jpeg(img, channels=input_shape[2])

    # Resize to 224 x 224 x 3 tensor
    img = tf.image.resize_with_pad(img, input_shape[0], input_shape[1])

    if is_tuning:
        img = tf.keras.applications.mobilenet.preprocess_input(img)
    else:
        img = tf.keras.applications.mobilenet.preprocess_input(img)
        img = tf.image.convert_image_dtype(img, tf.float32)[tf.newaxis, ...]

    return img


def dataset_prep_img(
    img_path: str,
    label_np: np.ndarray
) -> Tuple[EagerTensor, np.ndarray]:
    """
    Help function for create_dataset function
    """
    img = prep_img(img_path)
    return img, label_np


def create_dataset(
    filepaths: list,
    labels_np: np.array,
    batch_size: int = 50,
    is_training: bool = True
) -> PrefetchDataset:
    """
    Load and parse dataset.
    Source:
        https://towardsdatascience.com/multi-label-image-classification-in-tensorflow-2-0-7d4cf8a4bc72
    Args:
        filepaths (list):list of image paths
        labels_np (np.array): numpy array of shape (BATCH_SIZE, N_LABELS)
        batch_size (int, optional): Batch size. Defaults to 50.
        is_training (bool, optional): Boolean to indicate training mode. Defaults to True.

    Returns:
        PrefetchDataset: tensorflow pretetch dataset which can be used for training model
    """

    # Create a dataset of file paths and labels
    dataset = tf.data.Dataset.from_tensor_slices((filepaths, labels_np))
    # preprocess images in parallel
    dataset = dataset.map(dataset_prep_img, num_parallel_calls=tf.data.experimental.AUTOTUNE)

    if is_training:
        # This is a small dataset, only load it once, and keep it in memory.
        dataset = dataset.cache()
        # Shuffle the data each buffer size
        dataset = dataset.shuffle(buffer_size=1024)

    # Batch the data for multiple steps
    dataset = dataset.batch(batch_size)
    # Fetch batches in the background while the model is training.
    dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

    return dataset


def learning_curves(
    history: History,
    train_metrics: str,
    val_metrics: str
) -> Tuple[list, list, list, list]:
    """
    Plot the learning curves of loss and get metrics
    for the training and validation datasets.

    Args:
        history (History): model history
        train_metrics (str): name of train metric - for example 'binary_accuracy'
        val_metrics (str): name of train metric - for example 'val_binary_accuracy'

    Returns:
        Tuple[list,list,list,list]:
            - loss = Train loss
            - val_loss = Validation loss
            - train_m = Train metric (accuracy etc.)
            - val_m = Validation metrich (accuracy etc.)
    """

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    train_m = history.history[train_metrics]
    val_m = history.history[val_metrics]

    n_epochs = len(loss)

    style.use("bmh")
    plt.figure(figsize=(8, 8))

    plt.subplot(2, 1, 1)
    plt.plot(range(1, n_epochs+1), loss, label='Train. Loss')
    plt.plot(range(1, n_epochs+1), val_loss, label='Valid. Loss')
    plt.legend(loc='upper right')
    plt.ylabel('Loss')
    plt.title('Train. and Valid. Loss')

    plt.subplot(2, 1, 2)
    plt.plot(range(1, n_epochs+1), train_m, label='Train. ' + train_metrics)
    plt.plot(range(1, n_epochs+1), val_m, label='Valid. ' + train_metrics)
    plt.legend(loc='lower right')
    plt.ylabel(train_metrics)
    plt.title('Train. and Valid. ' + train_metrics)
    plt.xlabel('epoch')

    plt.show()

    return loss, val_loss, train_m, val_m


# def extract_feat(
#     img_path: str,
#     model,
#     input_shape: tuple = (224,224,3)):
#     """
#        It extracts feature vector of image from fine tuned model
#     Args:
#        img_paht: path to image
#        model: fine tuned model
#        input_shape: shape of image the model expect
#     """
#     img = prep_img(img_path=img_path, input_shape=input_shape, is_tuning=False)

#     #extract feat. vector
#     feat = model.predict(img)

#     return feat
